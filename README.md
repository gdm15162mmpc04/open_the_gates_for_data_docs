# puddle.

"puddle." is een responsive web-applicatie die gebruikers vertelt wat voor weer het is en ze advies geeft indien ze best wel of niet met de fiets in of rond Gent, België gaan rijden. Deze applicatie is gemaakt in opdracht van het van New media design & development voor de 2e examenkans in het academiejaar 2015 - 2016 door Lander Denys klas 2MMPb. Te vermelden valt dat de datasets in betrekking met het weer dagelijks na 17:20 kunnen offline gaan om ombekende reden. Dit is een probleem bij de datasets van stad Gent zelf.

# Dossier
[Link naar dossier.pdf](dossier.pdf)

# Dossier.md
[Link naar dossier.md](dossier.md)

# Poster
[Link naar poster.pdf](poster.pdf)

# Presentatie
[Link naar presentatie.pdf](presentatie.pdf)

# Screencast
[Link naar screencast](https://www.youtube.com/watch?v=a-Z36V1E3UE&feature=youtu.be)

# Screenshots
* [screenshot_320.png](screenshot_320.png)
* [screenshot_480.png](screenshot_480.png)
* [screenshot_640.png](screenshot_640.png)
* [screenshot_800.png](screenshot_800.png)
* [screenshot_960.png](screenshot_960.png)
* [screenshot_1024.png](screenshot_1024.png)
* [screenshot_1280.png](screenshot_1280.png)

# Timesheet
[Timesheet](timesheet.xlsx)
