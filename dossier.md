# Briefing & Analyse.
### Briefing

Maak een responsive mobile-first webapplicatie waarin minimaal 6 datasets,
afkomstig uit de dataset-pool van Stad Gent, verwerkt zijn. Conceptueel denken
is heel belangrijk in deze applicatie. Dit betekent dat jullie verbanden moeten
leggen tussen deze datasets. Het is louter niet alleen het oplijsten en visualiseren
van deze datasets, er moet een concept rond gebouwd worden.
Deze applicatie is bestemd voor +18 jarigen waarbij de gebruiksvriendelijkheid,
interactiviteit, uitbreidbaarheid en schaalbaarheid van belang zijn.

### Analyse

De webapp die ik bedacht heb is er een die de weersomstandigheden in Gent
weergeeft en daarop advies geeft indien het een goed idee is om te fietsen in
gent. Het is een mobile-first applicatie van de dataset-pool van Stad Gent. Ik heb
volgende 6 datasets gebruikt:

* Weersverwachting 1u
* Weersverwachting 3D
* Parken
* Blue bikes deelfietsen Gent Sint-Pieters
* Blue bikes deelfietsen Gent Dampoort
* Fietsdienstverlening (Geojson)

Hiermee besloot ik de website app genaamd “puddle.” te maken. Ik koos voor de
naam “puddle” omdat het geheel over regen gaat. De applicatie toont wat voor
weer het op het huidige moment is, wat voor weer het volgende uur zal zijn en
het gemiddelde weer voor de komende 3 dagen. Indien de bezoeker geen fiets
heeft, kan deze direct zien indien er nog fietsen beschikbaar zijn voor huur aan
het station van Gent Sint-Pieters of Gent Dampoort. Verder is er ook nog een
kaart met andere verhuurlocaties van fietsen indien de Blue Bikes allemaal zijn
uitgehuurd.

### Specificaties

* De gebruiker kan genieten van een simpele en duidelijke responsive one-page
slider website.
* Er wordt aan de hand van een groot icoon op de 2e pagina getoond wat voor
weer het is. Ook wordt er verteld indien het een goed of slecht idee is om te
gaan fietsen.
* Aan de hand van datasets worden de weersvoorspellingen ingeladen voor
het volgende uur, morgen, overmorgen en de dag daarna zodat men zich kan
voorbereiden indien ze ergens naar toe moeten.
* Indien men geen fiets heeft kan met altijd een fiets huren. Onder de
weersvoorspellingen staat er aan de hand van datasets hoeveel fietsen er nog
beschikbaar zijn om te huren aan 1 van de 2 treinstations in Gent.
* Om het vorige te verduidelijken vind men een kaart met locaties aangeduid
waar men fietsen kan huren tegen een lage prijs.
* Daaronder staat een contactformulier waarbij mensen ons kunnen laten
weten hoe we hun geholpen hebben, en een checkbox waardoor ze hun
kunnen inschrijven op onze nieuwsbrief.
* Als voorlaatste kan de bezoeker een kleine quiz maken zodat ze wat meer
kennis krijgen van het weer in België.
* Social media staan onderaan gelinkt zodat ze ons overal op kunnen volgen.
Op Facebook en Twitter komen leuke reacties van de mensen, op Instagram
komen sfeerfotos van het weer in Gent.
* Elke soort dataset is geordend in een ander javascript bestand zodat dit
overzichtelijk blijft.
* Er wordt gebruikt gemaakt van HTML/CSS, Javascript, JSON-datasets en
GEOJSON datasets, Google Maps en Mapbox voor kaarten en Bootstrap als
wireframe.

### Ideeënborden

* [Ideeënbord 1](images/ideeenbord1.png)
* [Ideeënbord 2](images/ideeenbord2.png)

### Wireframe -mailsjabloon

* [wireframe](images/wireframe.png)

### Wireflow Webapp

* [Wireflow homepage](images/wireflow1.png)
* [Wireflow weather page](images/wireflow2.png)
* [Wireflow contact page](images/wireflow3.png)
* [Wireflow quiz](images/wireflow4.png)

### Moodboard

* [Moodboard](images/moodboard1.png)

### Style Tiles

* [Style Tile 1](images/tile1.png)
* [Style Tile 2](images/tile2.png)
* [Style Tile 3](images/tile3.png)

### Visual Designs

* [Design homepage](images/design1.png)
* [Design weather page](images/design2.png)
* [Design quiz page](images/design3.png)
* [Design contact page](images/design4.png)

### Screenshots

* [html](images/html.png)
* [css](images/css.png)
* [Javascript](images/jv1.png)
* [Javascript](images/jv2.png)
* [Javascript](images/jv3.png)
* [Timesheet](timesheet.xlsx)

